# Installation Instructions

In order to participate and perform the programming exercises of the course, 
you need to have the following installed.

* Zoom Client
    - [Download Center](https://zoom.us/download#client_4meeting)
    - [Getting started with the Zoom web client](https://support.zoom.us/hc/en-us/articles/214629443-Getting-started-with-the-Zoom-web-client)
    - [Getting started on Windows](https://support.zoom.us/hc/en-us/articles/201362033-Getting-started-on-Windows-and-macOS)
    - [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)
* GIT Client
  - https://git-scm.com/downloads
* A BASH terminal, such as the GIT client terminal
* NodeJS / NPM
  - https://nodejs.org/en/download/
* Angular CLI
  - `npm install -g @angular/cli`
* A decent IDE, such as any of
    * MicroSoft Visual Code
        - https://code.visualstudio.com/
    * JetBrains WebStorm
        - https://www.jetbrains.com/webstorm/download
* A modern browser, such as any of
  - [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html)
  - [Mozilla FireFox](https://www.mozilla.org/sv-SE/firefox/new/)
  - [Microsoft Edge](https://www.microsoft.com/sv-se/windows/microsoft-edge)
