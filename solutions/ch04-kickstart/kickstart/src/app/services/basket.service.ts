import {Injectable} from '@angular/core';
import {Product} from "../domain/product.domain";

export class Item {
    private _count: number = 1;

    constructor(private _product: Product) {
        this._count = 1;
    }

    get id(): string {return this._product.id;}
    get name(): string {return this._product.name;}
    get image(): string {return this._product.image;}
    get price(): number {return this._product.price;}
    get count(): number {return this._count;}
    get total(): number {return this.count * this.price;}
    increment(): void {this._count++;}
    decrement(): void {this._count--;}
}


@Injectable({providedIn: 'root'})
export class BasketService {
    private _items: Item[] = [];

    get items(): Item[] {return this._items;}
    get empty(): boolean {return this.count === 0;}
    get count(): number {
        return this.items
            .map(p => p.count)
            .reduce((sum, cnt) => sum+cnt, 0);
    }
    get total(): number {
        return this.items
            .map(p => p.total)
            .reduce((sum, cost) => sum + cost, 0)
    }

    clear(): void {this._items = [];}

    insert(product: Product): void {
        let item = this._items.find(p => p.id === product.id);
        if (item) {
            item.increment();
        } else {
            this._items.push(new Item(product));
        }
    }

    remove(product: Product): void {
        let ix = this._items.findIndex(p => p.id === product.id);
        if (ix > -1) {
            this._items.splice(ix, 1)
        }
    }
}
