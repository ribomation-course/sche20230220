// "id": "9558ade0-0530-4bb3-984c-55c162e40c97",
//   "name": "bread",
//   "price": 18.33,
//   "image": "/img/bread.jpg"
export interface Product {
  id: string;
  name: string;
  price: number;
  image: string;
}
