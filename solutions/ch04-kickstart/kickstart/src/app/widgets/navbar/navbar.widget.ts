import { Component } from '@angular/core';

export interface Link {
    name: string;
    uri: string;
}

@Component({
  selector: 'navbar',
  templateUrl: './navbar.widget.html',
  styleUrls: ['./navbar.widget.scss']
})
export class NavbarWidget {
    readonly links: Link[] = [
        {name: 'home', uri: '/home'},
        {name: 'products listing', uri: '/products'},
        {name: 'shopping basket', uri: '/shopping-basket'},
    ];
}
