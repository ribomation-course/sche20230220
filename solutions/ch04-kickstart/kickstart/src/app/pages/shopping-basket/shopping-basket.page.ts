import { Component } from '@angular/core';
import {BasketService} from "../../services/basket.service";

@Component({
  selector: 'app-shopping-basket',
  templateUrl: './shopping-basket.page.html',
  styleUrls: ['./shopping-basket.page.scss']
})
export class ShoppingBasketPage {
    constructor(public basketSvc: BasketService) {}
}
