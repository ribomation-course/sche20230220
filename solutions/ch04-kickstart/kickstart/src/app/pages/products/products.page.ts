import { Component } from '@angular/core';
import {Observable} from "rxjs";
import {Product} from "../../domain/product.domain";
import {ProductsService} from "../../services/products.service";
import {BasketService} from "../../services/basket.service";

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss']
})
export class ProductsPage {
    products$: Observable<Product[]>;

    constructor(
        public productsSvc: ProductsService,
        public basketSvc: BasketService
    ) {
        this.products$ = productsSvc.findAll();
    }

}
