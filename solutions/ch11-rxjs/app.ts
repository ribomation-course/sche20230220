import { interval } from 'rxjs';
import { map } from 'rxjs/operators';

const numSecs = Number(process.argv[2] || 10);

const clock$ =
    interval(1000)
        .pipe(
            map(_ => new Date().toLocaleTimeString())
        );

const subscription = clock$.subscribe(value => {
    console.log('clock: %s', value);
});

setTimeout(() => {
    subscription.unsubscribe();
}, numSecs * 1000 + 100);
