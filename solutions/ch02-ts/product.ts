export class Product {
    //{"id":"6252e27b-2c86-4391-92e2-9b9e566cb024","name":"Pajero","price":1177.51,"city":"Leon"}
    constructor(
        public id: string,
        public name: string,
        public price: number,
        public city: string
    ) { }

    static fromDB(data: any): Product {
        return new Product(data.id, data.name, data.price, data.city)
    }

    toDB(): any {
        return Object.assign({}, this)
    }
}

