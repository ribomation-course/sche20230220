import {readFileSync, writeFileSync} from 'fs';
import {Product} from './product';

const LIMIT    = Number(process.argv[2] || 1100)
const json     = readFileSync('products.json').toString();
const products = JSON.parse(json)
                     .map((p: any) => Product.fromDB(p));
const result   = products.filter((p: Product) => p.price >= LIMIT)
                         .map((p: Product) => p.toDB());
writeFileSync('products-expensive.json', JSON.stringify(result, null, 3));
console.log('written %d records', result.length);
