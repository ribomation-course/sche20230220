import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {NumberStepperWidget} from "./widgets/number-stepper/number-stepper.widget";

@NgModule({
    declarations: [
        AppComponent,
        NumberStepperWidget
    ],
    imports: [
        BrowserModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
