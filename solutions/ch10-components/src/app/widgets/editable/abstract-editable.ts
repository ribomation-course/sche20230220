import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Property, PropertyType} from "./property";

@Component({template: ''})
export class AbstractEditable<T extends PropertyType> implements OnInit {
    editing: boolean = false;

    @Input('value') propertyValue: PropertyType = this.defaultValue;
    editableValue: PropertyType | undefined;

    @Input('name') propertyName: string = '';
    @Input('hint') hint: string = 'Click to edit';
    @Input('min') min: number = 0;
    @Input('max') max: number = 9999999;

    @Output('updated') updatedEmitter = new EventEmitter<Property>();

    ngOnInit(): void {
        if (!this.propertyName) {
            throw new Error('[editable-text] missing property name')
        }
        if (!this.propertyValue) {
            throw new Error('[editable-text] missing property va;ue')
        }
    }

    get empty(): boolean {
        const val: any = this.propertyValue;
        if (!val) return true;
        if (val instanceof String) {
            return val.toString().trim().length === 0;
        }
        if (val instanceof Number) {
            return val.toString().trim().length === 0;
        }
        return false;
    }

    edit() {
        this.editableValue = this.propertyValue;
        this.editing = true;
    }

    onKeyUp(evt: any) {
        if (evt.key === 'Escape') {
            this.revert();
        } else if (evt.key === 'Enter') {
            this.save();
        }
    }

    revert() {
        this.editing = false;
        this.editableValue = undefined;
    }

    save() {
        this.editing = false;
        this.propertyValue = this.editableValue as T;
        this.editableValue = undefined;
        this.updatedEmitter.emit(this.payload);
    }

    get fieldType(): string {
        return 'text';
    }

    get defaultValue(): PropertyType {
        return '';
    }

    get payload(): Property {
        return {
            name: this.propertyName,
            value: this.propertyValue || '',
        };
    }

}
