export type PropertyType = string | number;

export interface Property {
    name: string;
    value:  PropertyType;
}
