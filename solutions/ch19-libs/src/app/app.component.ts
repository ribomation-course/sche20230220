import {Component} from '@angular/core';
import {DateTime} from "luxon";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    get dates(): DateTime[] {
        const today = DateTime.now();
        const offset = 5;
        const units = ['seconds', 'minutes', 'hours', 'days'];

        return units
            .map(unit => ([
                today.minus({[unit]: offset}),
                today.plus({[unit]: offset + 1}),
            ]))
            .flat()
            .concat([today])
            .sort()
    }

}
