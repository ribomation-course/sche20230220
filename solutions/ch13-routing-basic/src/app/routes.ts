import {Route} from "@angular/router";
import {HomePage} from "./pages/home/home.page";
import {AboutPage} from "./pages/about/about.page";
import {ListPage} from "./pages/list/list.page";
import {NotFoundPage} from "./pages/not-found/not-found.page";

export const ROUTES: Route[] = [
    {path: 'home', component: HomePage, data: {
        title: 'The Home/Welcome/Start Page', nav: 'Start'
    }},
    {path: 'about', component: AboutPage, data: {title: 'About this Silly App', nav: 'About'}},
    {path: 'list', component: ListPage, data: {title: 'The Full Product List', nav: 'Products'}},
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: '**', component: NotFoundPage},
];

