import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styles: ['']
})
export class HomePage implements OnInit {
    constructor(private titleSvc: Title, private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        const title = this.route.snapshot.data['title'];
        this.titleSvc.setTitle(title);
    }
}
