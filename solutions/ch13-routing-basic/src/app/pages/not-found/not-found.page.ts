import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
      <h1>Oooops, page not found!</h1>
      <p>
          I didn't found the page you were looking for.
          Please, go back to <a [routerLink]="'/home'">HOME</a>
      </p>
  `,
  styles: ['']
})
export class NotFoundPage {}
