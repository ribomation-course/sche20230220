import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-about',
    templateUrl: './about.page.html',
    styles: [`
        section {
            display: flex;
            flex-direction: row;
            justify-content: space-around;
        }
    `]
})
export class AboutPage implements OnInit {
    names = [
        'angular', 'vue', 'react'
    ];

    constructor(private titleSvc: Title, private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        const title = this.route.snapshot.data['title'];
        this.titleSvc.setTitle(title);
    }

    url(name: string, size: number = 200): string {
        return `https://robohash.org/${name}.png?size=${size}x${size}&set=set2`;
    }

}
