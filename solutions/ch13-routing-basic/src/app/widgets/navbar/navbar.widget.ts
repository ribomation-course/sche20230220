import {Component} from '@angular/core';
import {ROUTES} from '../../routes'

interface Nav {
    uri: string;
    text: string;
}

@Component({
    selector: 'navbar',
    templateUrl: './navbar.widget.html',
    styleUrls: ['./navbar.widget.css']
})
export class NavbarWidget {
    navbar: Nav[] = ROUTES
        .filter(r => r.data && r.data['title'])
        .map(r => ({
            uri: `/${r.path}`,
            text: r.data && (r.data['nav'] || r.data['title'])
        } as Nav));
}
