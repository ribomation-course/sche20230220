import {Component} from '@angular/core';
import {map, Observable, switchMap} from "rxjs";
import {Product} from "../../../domain/product";
import {ProductsService} from "../../../services/products.service";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";

function strip(s: string) {
    return s.replace(/[^a-z]/g, '_');
}

function mkUrl(name:string) {
    const sz = 75;
    const hash = strip(name); 
    return `https://robohash.org/${hash}.png?size=${sz}x${sz}&set=set3`;
}

@Component({
    selector: 'app-detail',
    templateUrl: './detail.panel.html',
    styleUrls: ['./detail.panel.css']
})
export class DetailPanel {
    product$: Observable<Product>;
    readonly url = '';

    constructor(private productSvc: ProductsService,
                private route: ActivatedRoute,
                private titleSvc: Title
    ) {
        this.product$ = route.paramMap
            .pipe(
                map((params: ParamMap) => Number(params.get('id'))),
                switchMap((id: number) => productSvc.findById(id)),
                map((p:Product) => {
                    p.image = mkUrl(p.name);
                    const title = `Product ID=${p.id}, Name=${p.name}`
                    titleSvc.setTitle(title);
                    return p;
                })
            )
    }
}
