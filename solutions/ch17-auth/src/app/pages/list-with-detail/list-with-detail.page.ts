import {Component} from '@angular/core';
import {Observable} from "rxjs";
import {Product} from "../../domain/product";
import {ProductsService} from "../../services/products.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-list-with-detail',
    templateUrl: './list-with-detail.page.html',
    styleUrls: ['./list-with-detail.page.css']
})
export class ListWithDetailPage {
    products$: Observable<Product[]>;

    constructor(private productSvc: ProductsService, private router: Router) {
        this.products$ = productSvc.findAll();
    }

    async showProduct(id: number) {
        await this.router.navigate(['list-detail', 'detail', id]);
    }
}
