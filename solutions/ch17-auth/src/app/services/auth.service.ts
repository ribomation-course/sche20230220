import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {catchError, map, Observable, of} from "rxjs";

export interface LoginResponse {
    token: string;
    username: string;
    date: string;
}

@Injectable({providedIn: 'root'})
export class AuthService {
    private readonly url = 'http://localhost:3000/auth/login';
    private current: LoginResponse | undefined;

    constructor(private httpSvc: HttpClient) {}

    login(username: string, password: string): Observable<boolean> {
        return this.httpSvc
            .post<LoginResponse>(this.url, {username, password})
            .pipe(
                map(data => {
                    this.current = data;
                    console.log('[auth] SUCCESS: %o', data)
                    return true;
                }),
                catchError((err: HttpErrorResponse) => {
                    console.log('[auth] FAILED: %d %s; %s', err.status, err.statusText, err.message);
                    this.current = undefined;
                    return of(false);
                })
            );
    }
    
    getToken(): string {
        if (!!this.current) {
            return this.current.token;
        }
        throw new Error('not logged in');
    }

    isAuthenticated(): boolean {
        return !!this.current;
    }
    
    logout():void {
        this.current = undefined;
    }
    
}
