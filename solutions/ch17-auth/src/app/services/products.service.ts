import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, of} from "rxjs";
import {Product} from "../domain/product";
import {AuthService} from "./auth.service";

@Injectable({providedIn: 'root'})
export class ProductsService {
    private readonly url = 'http://localhost:3000/products';

    constructor(private http: HttpClient, private authSvc: AuthService) {
    }

    findAll(): Observable<Product[]> {
        return this.http.get<Product[]>(this.url, {headers: this.headers})
            .pipe(
                catchError(x => {
                    console.error('[PROD] %d "%s" "%s"', x.status, x.message, x.error)
                    return of([]);
                })
            );
    }

    findById(id: number): Observable<Product> {
        return this.http.get<Product>(`${this.url}/${id}`, {headers: this.headers})
            .pipe(
                catchError(x => {
                    console.error('[PROD] %d "%s" "%s"', x.status, x.message, x.error)
                    return of({id: -1, name: '', price: ''} as Product);
                })
            );
    }

    get headers(): HttpHeaders {
        return new HttpHeaders().set('X-Auth-Token', this.authSvc.getToken());
    }
}
