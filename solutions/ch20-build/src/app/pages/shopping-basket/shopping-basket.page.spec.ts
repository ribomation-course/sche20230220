import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoppingBasketPage } from './shopping-basket.page';

describe('ShoppingBasketPage', () => {
  let component: ShoppingBasketPage;
  let fixture: ComponentFixture<ShoppingBasketPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShoppingBasketPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShoppingBasketPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
