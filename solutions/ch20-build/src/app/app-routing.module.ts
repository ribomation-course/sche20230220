import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {WelcomePage} from "./pages/welcome/welcome.page";
import {ProductsPage} from "./pages/products/products.page";
import {ShoppingBasketPage} from "./pages/shopping-basket/shopping-basket.page";

const routes: Routes = [
    {path: 'home',            component: WelcomePage},
    {path: 'products',        component: ProductsPage},
    {path: 'shopping-basket', component: ShoppingBasketPage},
    {path: '',                redirectTo: '/home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
