import {Component} from '@angular/core';
import {BasketService} from "../../services/basket.service";

@Component({
    selector: 'basket-widget',
    templateUrl: './basket.widget.html',
    styleUrls: ['./basket.widget.scss']
})
export class BasketWidget {
    constructor(public basketSvc: BasketService) {}
}
