import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasketWidget } from './basket.widget';

describe('BasketWidget', () => {
  let component: BasketWidget;
  let fixture: ComponentFixture<BasketWidget>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BasketWidget ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BasketWidget);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
