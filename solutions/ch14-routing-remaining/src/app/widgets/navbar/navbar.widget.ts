import {Component, OnInit} from '@angular/core';
import {ROUTES} from '../../routes'
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {filter, map, mergeMap} from "rxjs";
import {Title} from "@angular/platform-browser";

interface Nav {
    uri: string;
    text: string;
}

@Component({
    selector: 'navbar',
    templateUrl: './navbar.widget.html',
    styleUrls: ['./navbar.widget.css']
})
export class NavbarWidget implements OnInit {
    navbar: Nav[] = ROUTES
        .filter(r => r.data && r.data['title'])
        .filter(r => r.data && !r.data['hide'])
        .map(r => ({
            uri: `/${r.path}`,
            text: r.data && r.data['title']
        } as Nav));

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private titleSvc: Title
    ) {
    }

    ngOnInit(): void {
        //https://stackoverflow.com/a/58654690
        this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                map(() => this.route),
                map(route => {
                    while (route.firstChild) route = route.firstChild;
                    return route;
                }),
                filter(route => route.outlet === 'primary'),
                mergeMap(route => route.data), //https://www.javatpoint.com/rxjs-mergemap-transformation-operator
                map(data => data['title'])
            )
            .subscribe(title => {
                    this.titleSvc.setTitle(title);
                }
            )
    }

}
