# Ch. 18 - Configuration

With Angular version 15, there are no `src/environment/*` files. However,
it's easy to bring it back. This exercise shows how.

## Prepare

    npm install

## Configuration
Because we are setting it up from scratch, we are free to choose the details.
So, why not give it a better name.

### Create a directory for our configuration files

    `mkdir -p src/config`

### Create the first config file and populate it

    touch src/config/config.ts
    cat<<EOT
    export const config = {
        mode: 'development',
        robotUrl: 'https://robohash.org/dev.png?set=set2'
    };
    EOT

### Create the second config file

    touch src/config/config.test.ts
    cat<<EOT
    export const config = {
        mode: 'test',
        robotUrl: 'https://robohash.org/test.png?set=set2'
    };
    EOT

### Create the third config file

    touch src/config/config.prod.ts
    cat<<EOT
    export const config = {
        mode: 'production',
        robotUrl: 'https://robohash.org/prod.png?set=set2'
    };
    EOT

### Edit `./angular.json`

Within `projects/appname/architect/build/configurations` add the following
to `production`

    "fileReplacements": [
        {"replace": "src/config/config.ts", "with": "src/config/config.prod.ts"}
    ],

Insert a new configuration, for `test`, below `production`

    "test": {
        "fileReplacements": [
            {"replace": "src/config/config.ts", "with": "src/config/config.test.ts"}
        ]
    },

Within `projects/appname/architect/serve/configurations`, insert a new
configuration for `test`

    "test": {
        "browserTarget": "18-configuration:build:test"
    },


## Edit `app.component.ts`

Import the config object, that will be replaced for the other configuration 

    import {config} from '../config/config'

Replace the content of the class with

    mode = config.mode;
    robot = config.robotUrl;

## Edit `app.component.html`

Replace the content with

    <h1>Demo of ng app configuration</h1>
    <p>Configuration name: <kbd>{{mode}}</kbd></p>
    <p><img [src]="robot"  alt="robot"></p>


## Launch the dev servers, for each config

    ng serve --open --configuration=development --port=4100
    ng serve --open --configuration=test --port=4200
    ng serve --open --configuration=production --port=4300


## Build, for each config

    ng build --configuration=development
    ng build --configuration=test
    ng build --configuration=production

