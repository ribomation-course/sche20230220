import {Component} from '@angular/core';
import {LoggerService} from "./services/logger.service";
import {UsersService} from "./services/users.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    user: any = {};

    constructor(private logger: LoggerService, private userSvc: UsersService) {
        logger.print('App started')
        userSvc.fetchOne(1)
            .subscribe(data => {
                this.user = data;
            })
    }

    msg(txt: string = 'Tjabba Habba') {
        this.logger.print(txt)
    }
}
