import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {UsersService} from "./services/users.service";
import {MockUsersService} from "./services/MockUsersService";

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
    ],
    providers: [
        {
            provide: UsersService, useClass: MockUsersService
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
