import {UsersService} from "./users.service";
import {Observable, of} from "rxjs";
import {Injectable} from "@angular/core";

@Injectable()
export class MockUsersService extends UsersService {
    readonly fakeData: any = [
        {
            "id": 1,
            "name": "Nisse Hult",
            "username": "nisse",
            "email": "nisse@gmail.com",
            "address": {
                "street": "Nissegatan 3",
                "city": "Knäckebröhult",
                "zipcode": "555 66",
                "geo": {
                    "lat": "-37.3159",
                    "lng": "81.1496"
                }
            },
        },
        {
            "id": 2,
            "name": "Anna Conda",
            "username": "anna",
            "email": "anna@gmail.com",
            "address": {
                "street": "Annagatan 7",
                "city": "Köttbullsta",
                "zipcode": "9999 88",
                "geo": {
                    "lat": "-43.9509",
                    "lng": "-34.4618"
                }
            },
        }
    ];

    override fetchAll(): Observable<any> {
        return of(this.fakeData)
    }

    override fetchOne(id: number): Observable<any> {
        const result = this.fakeData.find((obj: any) => obj.id === id);
        if (result) {
            return of(result);
        } else {
            return of([])
        }
    }
}
