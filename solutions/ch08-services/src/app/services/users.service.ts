import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class UsersService {
    readonly url: string = 'https://jsonplaceholder.typicode.com/users';

    constructor(private http: HttpClient) {}

    fetchOne(id: number): Observable<any> {
        return this.http.get(`${this.url}/${id}`);
    }

    fetchAll() :Observable<any> {
        return this.http.get(this.url);
    }
}
