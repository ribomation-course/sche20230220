import {Component} from '@angular/core';
import {ProductsService} from "./services/products.service";
import {Observable} from "rxjs";
import {Product} from "./domain/product";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  products$: Observable<Product[]>;
  product: Product | undefined;

  constructor(private productSvc: ProductsService) {
    this.products$ = productSvc.findAll();

    productSvc.findById(3)
      .subscribe(obj => this.product = obj);
  }
}
