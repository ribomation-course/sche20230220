# Angular Basics 
###  2023 February

Welcome to this course that will get you up to speed with web app development using Angular.

# Links

* [Installation Instructions](./installation-instructions.md)
* [Course Details](https://www.ribomation.se/programmerings-kurser/web/angular-basics/)
* [Zoom Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)


# Usage of this GIT Repo
Ensure you have a [GIT client](https://git-scm.com/downloads) installed and clone this repo. 

    mkdir -p ~/angular-course/my-solutions
    cd ~/angular-course
    git clone <https url to this repo> gitlab

![GIT HTTPS URL](./img/git-url.png)

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/angular-course/gitlab
    git pull


# Build Solution/Demo Programs
First, copy the content of an app directory to a fresh folder, outside the gitlab folder.
This is to ensure you don't get merge conflicts the next time you perform a `git pull`.

Second, open a terminal window in the new directory and install all
dependencies with the command below.

    npm install

Then you can launch the app, using either of

    npm run start
    npx ng serve --open

# Videos & Articles

* [What's New in Angular v15, by Minko Gechev](https://youtu.be/-n0WhqabSmc)
* Angular's Future with Standalone Components (6 parts)
    1. [Lightweight Solutions Using Standalone Components](https://www.angulararchitects.io/en/aktuelles/angulars-future-without-ngmodules-lightweight-solutions-on-top-of-standalone-components/)
    1. [What Does That Mean for Our Architecture?](https://www.angulararchitects.io/en/aktuelles/angulars-future-without-ngmodules-part-2-what-does-that-mean-for-our-architecture/)
    1. [4 Ways to Prepare for Angular’s Upcoming Standalone Components](https://www.angulararchitects.io/en/aktuelles/4-ways-to-prepare-for-angulars-upcoming-standalone-components/)
    1. [Routing and Lazy Loading with Angular’s Standalone Components](https://www.angulararchitects.io/en/aktuelles/routing-and-lazy-loading-with-standalone-components/)
    1. [Angular Elements: Web Components with Standalone Components](https://www.angulararchitects.io/en/aktuelles/angular-elements-web-components-with-standalone-components/)
    1. [The Refurbished HttpClient in Angular 15](https://www.angulararchitects.io/en/aktuelles/the-refurbished-httpclient-in-angular-15-standalone-apis-and-functional-interceptors/)




***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
