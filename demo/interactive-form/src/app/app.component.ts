import {Component} from '@angular/core';
import {FormControl} from "@angular/forms";
import {debounceTime, distinctUntilChanged, Observable, of, switchMap} from "rxjs";
import {HttpClient, HttpParams} from "@angular/common/http";

interface Language {
    name: string;
    inventor: string;
    year: number;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    searchField: FormControl = new FormControl('');
    results$: Observable<Language[] | undefined>;
    readonly url = 'http://localhost:3000/languages';

    constructor(private httpSvc: HttpClient) {
        this.results$ = this.searchField.valueChanges
            .pipe(
                debounceTime(500),
                distinctUntilChanged(),
                switchMap(txt => this.sendSearchRequest(txt))
            )
    }

    sendSearchRequest(phrase: string): Observable<Language[] | undefined> {
        if (!phrase || phrase.trim().length === 0) {
            return of(undefined);
        }

        const params = new HttpParams().set('q', phrase);
        return this.httpSvc.get<Language[]>(this.url, {params});
    }

}
