import {AboutPage} from './pages/about.page';

describe('Page: About', () => {
  let page: AboutPage;

  beforeEach(() => {
    page = new AboutPage();
    page.navigateTo();
  });

  it('should display header', () => {
    expect(page.getHeaderText()).toContain('...');
  });

  it('should display the app name', () => {
    expect(page.getNameText()).toContain('Demo');
  });

});
