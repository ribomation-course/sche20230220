import { TestBed, inject } from '@angular/core/testing';

import { ProductDaoService } from './product-dao.service';

describe('ProductDaoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductDaoService]
    });
  });

  it('should be created', inject([ProductDaoService], (service: ProductDaoService) => {
    expect(service).toBeTruthy();
  }));
});
