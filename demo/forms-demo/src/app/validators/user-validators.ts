import {
  AbstractControl,
  FormGroup, ValidationErrors,
  ValidatorFn
} from '@angular/forms';

export function multipleOf(n: number): ValidatorFn {
  return (field: AbstractControl): ValidationErrors | null => {
    if (n === undefined) return null;

    n = +n; //ensure it's a number
    if (n < 2) throw new Error('Invalid multiple: ' + n);

    if ((field.value % n) === 0) return null;
    return {
      multipleOf: {value: field.value, multiple: n}
    };
  };
}

export function equals(field1Name: string, field2Name: string): ValidatorFn {
  return (form: FormGroup): ValidationErrors | null => {
    const field1 = form.controls[field1Name];
    const field2 = form.controls[field2Name];

    //skip, if not defined yet or already has errors
    if (!field1 || !field2) return null;
    if (field1.errors || field2.errors) return null;

    if (field1.value === field2.value) {
      field2.setErrors(null);
      return null;
    }

    field2.setErrors({
      equals: {field1: field1.value, field2: field2.value,}
    });
    return {};
  };
}

