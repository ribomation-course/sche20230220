import {Component} from "@angular/core";

@Component({
  selector: "app-demo-traincase",
  template: `
    <p>plain: {{txt}}</p>
    <p>train: {{txt | titlecase | Train_Case}}</p>
  `,
  styles: []
})
export class DemoTraincaseComponent {
  txt = "foobar strikes again";
}
