import {Component} from "@angular/core";

@Component({
  selector: "app-demo-text-transformations",
  template: `
    <table>
      <tr><th>plain</th><td>{{txt}}</td></tr>
      <tr><th>upper</th><td>{{txt | uppercase}}</td></tr>
      <tr><th>lower</th><td>{{txt | lowercase}}</td></tr>
      <tr><th>title</th><td>{{txt | titlecase}}</td></tr>
      <tr><th>slice</th><td>{{txt | slice:3:13}}</td></tr>
    </table>
  `,
  styles: [`table {width:20rem;}`]
})
export class DemoTextTransformationsComponent {
  txt = "foobar strikes again";
}

