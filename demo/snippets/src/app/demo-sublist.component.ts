import {Component} from "@angular/core";

@Component({
  selector: "app-demo-sublist",
  template: `
    <div>
      Some prime numbers:
      <ul>
        <li *ngFor="let p of primes | slice:2:7; index as ix">{{ix+1}}: {{p}}</li>
      </ul>
    </div>
  `,
  styles: []
})
export class DemoSublistComponent {
  primes: number[] = [
    2, 3, 5, 7, 11, 13, 17, 19, 23
  ];
}
