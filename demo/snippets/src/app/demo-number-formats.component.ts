import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-number-formats',
  template: `
    <table>
      <tr><th>plain</th>   <td>{{value}}</td></tr>
      <tr><th>number</th>  <td>{{value | number}}</td></tr>
      <tr><th>number</th>  <td>{{value | number:'3.0-4'}}</td></tr>
      <tr><th>percent</th> <td>{{value*0.01 | percent}}</td></tr>
      <tr><th>percent</th> <td>{{value*0.01 | percent:'1.1-1'}}</td></tr>
      <tr><th>currency</th><td>{{value*100 | currency}}</td></tr>
      <tr><th>currency</th><td>{{value*100 | currency:'SEK'}}</td></tr>
      <tr><th>currency</th><td>{{value*100 | currency:'EUR':true:'1.0-0'}}</td></tr>
    </table>
  `,
  styles: [`table {width:20rem;}`]
})
export class DemoNumberFormatsComponent {
  value: number = 4 * Math.atan(1);
}
