import { Component } from '@angular/core';

interface Expense {
  date: Date;
  amount: number;
  type: 'lodging' | 'travel' | 'meal';
  hotel?: string;
  flight?: string;
  restaurant?: string;
}

@Component({
  selector: 'app-using-switch',
  templateUrl: './using-switch.component.html',
  styleUrls: ['./using-switch.component.css']
})
export class UsingSwitchComponent {
  expenses: Expense[] = [
    {
      date: new Date(2019, 5, 15, 10, 15),
      type: 'lodging',
      amount: 1500,
      hotel: 'Royal Plaza'
    },
    {
      date: new Date(2019, 5, 12, 12, 25),
      type: 'travel',
      amount: 3000,
      flight: 'SK1234'
    },
    {
      date: new Date(2019, 5, 14, 20, 5),
      type: 'meal',
      amount: 850,
      restaurant: 'The Dodgers Inn'
    },
  ];
  expense: Expense;
  index: number = 0;

  constructor() {
    this.expense = this.expenses[this.index];
  }

  toggle() {
    this.index = (this.index + 1) % this.expenses.length;
    this.expense = this.expenses[this.index];
  }
}

