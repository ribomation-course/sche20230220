import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [`
    :host {
      font-family: sans-serif;
    }
  `]
})
export class AppComponent {
}
