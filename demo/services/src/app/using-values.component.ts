import {Component, Inject} from "@angular/core";
import {AWS, BACKEND_URI} from "./app.consts";

@Component({
  selector: "using-values",
  template: `
    <div>URI: {{baseUri}}</div>
    <div>AWS: {{awsCreds | json}}</div>
  `,
  styles: []
})
export class UsingValuesComponent {
  baseUri: string;
  awsCreds: any;
  constructor(@Inject(BACKEND_URI) private uri: string,
              @Inject(AWS) private aws: any) {
    this.baseUri = uri;
    this.awsCreds = aws;
  }
}
