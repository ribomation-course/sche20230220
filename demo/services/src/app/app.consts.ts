import {InjectionToken} from "@angular/core";

export const BACKEND_URI = new InjectionToken<string>('backend-uri');
export const AWS         = new InjectionToken<string>('aws-creds');

