import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second',
  template: `
    <p>This is Second </p>
  `,
})
export class SecondComponent implements OnInit {
  constructor() { }
  ngOnInit() {}
}
