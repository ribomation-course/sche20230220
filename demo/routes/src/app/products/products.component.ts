import {Component} from "@angular/core";

@Component({
  template: `
    <div>
      <h2>Products Goes Here</h2>
      <router-outlet></router-outlet>
      <p>Child route view above this line.
        <a routerLink="overview">View Description</a>
        <a routerLink="spec">View Specifications</a>
      </p>
    </div>
  `,
})
export class ProductsComponent {}
