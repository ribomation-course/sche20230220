import {Injectable} from '@angular/core';

export interface Token {
  username: string;
  value: string;
  date: string;
}

@Injectable()
export class AuthService {
  public static readonly TOKEN_KEY = 'auth.service.token';

  public login(usr: string, pwd: string): boolean {
    if (usr && pwd) {
      //...check usr & pwd somehow...
      let token: Token = {
        username: usr,
        value: '1234567890qwertyu',
        date: new Date().toUTCString()
      };
      sessionStorage.setItem(AuthService.TOKEN_KEY, JSON.stringify(token));
      return true;
    } else {
      return false;
    }
  }

  public isAuthenticated(): boolean {
    let t = sessionStorage.getItem(AuthService.TOKEN_KEY);
    return !!t;
  }

  public logout(): void {
    sessionStorage.removeItem(AuthService.TOKEN_KEY);
  }

  public getUsername(): string {
    let data = sessionStorage.getItem(AuthService.TOKEN_KEY);
    if (!data) {return undefined;}

    let token: Token = JSON.parse(data);
    return token.username;
  }
}

