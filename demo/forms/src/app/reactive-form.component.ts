import {Component, OnInit} from "@angular/core";
import {FormControl} from "@angular/forms";
import {FakeSearchService} from "./fake-search.service";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";

@Component({
  selector: "reactive-form",
  template: `
    <div>
      <h2>Reactive Form Field</h2>
      <input type="search" [formControl]="phrase" placeholder="Enter search phrase"/>
      <h3 *ngIf="(results|async)?.length > 0">Results</h3>
      <ul>
        <li *ngFor="let r of results|async" [innerHTML]="r | keepHtml"></li>
      </ul>
    </div>
  `,
})
export class ReactiveFormComponent implements OnInit {
  phrase: FormControl;
  results: Observable<string[]>;

  constructor(private searchSvc: FakeSearchService) {
    this.phrase = new FormControl();
  }

  ngOnInit() {
    this.results = this.phrase
      .valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .map(txt => this.searchSvc.search(txt))
      .map(results => {
        const P = this.phrase.value;
        return results
          .map(res => res.replace(new RegExp(P, 'i'),
                                 `<span style="color:orangered">$&</span>`))
      })
    ;
  }
}
