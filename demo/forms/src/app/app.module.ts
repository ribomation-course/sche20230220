import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ModelDrivenFormComponent } from './model-driven-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ReactiveFormComponent } from './reactive-form.component';
import { FakeSearchService } from './fake-search.service';
import { KeepHtmlPipe } from './keep-html.pipe';
import { TemplateDrivenFormComponent } from './template-driven-form.component';

@NgModule({
  declarations: [
    AppComponent,
    ModelDrivenFormComponent,
    ReactiveFormComponent,
    KeepHtmlPipe,
    TemplateDrivenFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [FakeSearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
